<?php

namespace AppBundle\Component\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractController extends Controller
{
    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->get('doctrine.orm.default_entity_manager');
    }
}
