<?php

namespace AppBundle\Controller;

use AppBundle\Component\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {

    }
}
