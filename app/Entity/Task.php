<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     *
     * @var integer
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @var string
     */
    public $name;

    /**
     * @ORM\Column(type="string")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Type")
     * @ORM\JoinColumn(name="title", referencedColumnName="name")
     *
     * @var string
     */
    public $type;

    /**
     * @ORM\Column(type="string")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumn(name="title", referencedColumnName="id")
     *
     * @var string
     */
    public $category;

}