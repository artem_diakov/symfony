<?php

namespace AppBundle;

use Doctrine\Bundle as DoctrineBundle;
use RunetId\ApiClientBundle\RunetIdApiClientBundle;
use Ruvents\DoctrineFixesBundle\RuventsDoctrineFixesBundle;
use Ruvents\I18nRoutingBundle\RuventsI18nRoutingBundle;
use Sensio\Bundle as SensioBundle;
use Symfony\Bundle as SymfonyBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = [
            // Symfony Framework
            new SymfonyBundle\FrameworkBundle\FrameworkBundle(),
            new SensioBundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new SensioBundle\DistributionBundle\SensioDistributionBundle(),
            // Security
            new SymfonyBundle\SecurityBundle\SecurityBundle(),
            // Database
            new DoctrineBundle\DoctrineBundle\DoctrineBundle(),
            new DoctrineBundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new RuventsDoctrineFixesBundle(),
            // Templating
            new SymfonyBundle\TwigBundle\TwigBundle(),
            // Logs
            new SymfonyBundle\MonologBundle\MonologBundle(),
            // Mailing
            new SymfonyBundle\SwiftmailerBundle\SwiftmailerBundle(),
            // Routing
            new RuventsI18nRoutingBundle(),
            // RUNET-ID
            new RunetIdApiClientBundle(),
            // App
            new AppBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new SymfonyBundle\DebugBundle\DebugBundle();
            $bundles[] = new SymfonyBundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new SensioBundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    /**
     * {@inheritdoc}
     */
    public function getRootDir()
    {
        return __DIR__;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    /**
     * {@inheritdoc}
     */
    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    /**
     * {@inheritdoc}
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(dirname(__DIR__).'/config/config_'.$this->getEnvironment().'.yml');
    }
}
